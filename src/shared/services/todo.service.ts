import { TodoStrategy } from './todo-strategy/todo.strategy';
import { TodoType, TodoItem } from '../../pages/todos/todo.model';
import { TodoBigStrategy } from './todo-strategy/todo-big.strategy';
import { TodoMediumStrategy } from './todo-strategy/todo-medium.strategy';
import { TodoSmallStrategy } from './todo-strategy/todo-small.strategy';
import { TodoDefaultStrategy } from './todo-strategy/todo-default.strategy';

export class TodoStateService {

    private strategy: TodoStrategy;
    
    private todoType: TodoType;
    
    constructor() {
        this.initialize();
    }

    public setTodoType(type: TodoType = null) {
        this.todoType = type;
        this.initialize();
    }

    public getTodoType(): TodoType {
        return this.todoType;
    }

    public getTodos(): TodoItem[] {
        console.log('TodoService getTodos');
        return this.strategy.getTodos();
    }

    private initialize() {
        switch (this.todoType) {
            case TodoType.BIG_TASK:
            this.strategy = new TodoBigStrategy();
            break;
            case TodoType.MEDIUM_TASK:
            this.strategy = new TodoMediumStrategy();
            break;
            case TodoType.SMALL_TASK:
            this.strategy = new TodoSmallStrategy();
            break;
            default:
            this.strategy = new TodoDefaultStrategy();
            break;
        }
    }
}