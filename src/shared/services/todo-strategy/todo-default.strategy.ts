import { TodoStrategy } from './todo.strategy';
import { TodoItem, TodoType} from '../../../pages/todos/todo.model';

export class TodoDefaultStrategy extends TodoStrategy {

    constructor() {
        super();
        console.log('TodoDefaultStrategy');
    }

    public getTodos(): TodoItem[] {
        const result = [
            { id: 1, name: 'Wash dishes', type: TodoType.BIG_TASK, priority: 1 },
            { id: 1, name: 'Wash dishes', type: TodoType.MEDIUM_TASK, priority: 1 }, { id: 2, name: 'Laundry', type: TodoType.MEDIUM_TASK, priority: 1 }, { id: 3, name: 'Watch movie', type: TodoType.MEDIUM_TASK, priority: 1 },
            { id: 1, name: 'Wash dishes', type: TodoType.SMALL_TASK, priority: 1 }, { id: 2, name: 'Laundry', type: TodoType.SMALL_TASK, priority: 1 }, { id: 3, name: 'Watch movie', type: TodoType.SMALL_TASK, priority: 1 }, { id: 4, name: 'Laundry', type: TodoType.SMALL_TASK, priority: 1 }, { id: 5, name: 'Watch movie', type: TodoType.SMALL_TASK, priority: 1 },
            { id: 1, name: 'Wash dishes', type: TodoType.SMALL_TASK, priority: 1 }, { id: 2, name: 'Laundry', type: TodoType.SMALL_TASK, priority: 1 }, { id: 3, name: 'Watch movie', type: TodoType.SMALL_TASK, priority: 1 }, { id: 4, name: 'Laundry', type: TodoType.SMALL_TASK, priority: 1 }, { id: 5, name: 'Watch movie', type: TodoType.SMALL_TASK, priority: 1 }
          ];
        return result;
    }
}