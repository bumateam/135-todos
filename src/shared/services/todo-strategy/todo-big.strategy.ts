import { TodoStrategy } from './todo.strategy';
import { TodoItem, TodoType, TodoStatusEnum } from '../../../pages/todos/todo.model';

export class TodoBigStrategy extends TodoStrategy {

    constructor() {
        super();
        console.log('TodoBigStrategy');
    }

    public getTodos(): TodoItem[] {
        const result = [
            { id: 1, name: 'Wash dishes', type: TodoType.BIG_TASK, priority: 1, status: TodoStatusEnum.NEW },
            { id: 1, name: 'Wash dishes', type: TodoType.BIG_TASK, priority: 1, status: TodoStatusEnum.NEW },
            { id: 2, name: 'Laundry', type: TodoType.BIG_TASK, priority: 1, status: TodoStatusEnum.FINISHED },
            { id: 3, name: 'Watch movie', type: TodoType.BIG_TASK, priority: 1, status: TodoStatusEnum.NEW },
          ];
        return result;
    }
}