import { TodoStrategy } from './todo.strategy';
import { TodoItem } from '../../../pages/todos/todo.model';

export class TodoSmallStrategy extends TodoStrategy {

    constructor() {
        super();
        console.log('TodoSmallStrategy');
    }

    public getTodos(): TodoItem[] {
        return null;
    }
}