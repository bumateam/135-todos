import { TodoStrategy } from './todo.strategy';
import { TodoItem } from '../../../pages/todos/todo.model';

export class TodoMediumStrategy extends TodoStrategy {

    constructor() {
        super();
        console.log('TodoMediumStrategy');
    }

    public getTodos(): TodoItem[] {
        return null;
    }
}