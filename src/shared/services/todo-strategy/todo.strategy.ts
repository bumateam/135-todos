import { TodoItem } from '../../../pages/todos/todo.model';

export abstract class TodoStrategy {
    public abstract getTodos(): TodoItem[];
}