import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StartupPageEnum as pageEnum } from '../../enums/startup-page.enum';

@Injectable()
export class StartupProvider {

  constructor(private storage: Storage) {
  }

  showWelcome(): Promise<boolean> {
    return this.showPage(pageEnum.WELCOME_PAGE);
  }

  showTutorial(): Promise<boolean> {
    return this.showPage(pageEnum.TUTORIAL_PAGE);
  }

  private async showPage(pageEnum: pageEnum): Promise<boolean> {
    let result = false;
    if (!await this.storage.get(pageEnum)) {
      result = true;
      this.storage.set(pageEnum, true);
    }

    return result;
  }
}
