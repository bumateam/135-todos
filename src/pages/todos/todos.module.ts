import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskItemModule } from '../../components/task-item/task-item.module';
import { TodosPage } from './todos';

@NgModule({
  declarations: [
    TodosPage,
  ],
  imports: [
    IonicPageModule.forChild(TodosPage),
    TaskItemModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TodosPageModule {}
