import {Component, ViewChild, OnChanges, NgZone} from '@angular/core';
import {Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import { TodoItem, TodoType } from './todo.model';
import { TodosBigTasksPage } from '../../pages/todos-big-tasks/todos-big-tasks';
import { TodosMediumTasksPage } from '../../pages/todos-medium-tasks/todos-medium-tasks';
import { TodosSmallTasksPage } from '../../pages/todos-small-tasks/todos-small-tasks';
import { TodoStateService } from '../../shared/services/todo.service';

interface TodosFooterButton {
  id: number;
  name: string;
  type: TodoType,
  page: any;
  class: string;
}

const TODOS_FOOTER_BUTTONS: TodosFooterButton[] = [
  { id: 3, name: '5', type: TodoType.SMALL_TASK, page: TodosSmallTasksPage , class: 'small-task'},
  { id: 2, name: '3', type: TodoType.MEDIUM_TASK, page: TodosMediumTasksPage, class: 'medium-task' },
  { id: 1, name: '1', type: TodoType.BIG_TASK, page: TodosBigTasksPage, class: 'big-task' },
];

@IonicPage()
@Component({
  selector: 'page-todos',
  templateUrl: 'todos.html',
})
export class TodosPage {

  @ViewChild(Content) content: Content;
  public todos: TodoItem[];
  public footerButtons: TodosFooterButton[] = TODOS_FOOTER_BUTTONS;
  hideAddButton = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private todoService: TodoStateService,
    private zone: NgZone) {
  }

  ionViewWillEnter() {
    console.log('TodosPage ionViewWillEnter');
    this.todoService.setTodoType()
  }

  ionViewDidEnter() {
    console.log('TodosPage ionViewDidEnter');
    this.todos = this.todoService.getTodos();
  }

  navigate(footerButton: TodosFooterButton) {
    this.todoService.setTodoType(footerButton.type);
    this.navCtrl.push(footerButton.page, { action: 'new' });
  }

  contentScroll(event: any) {
    this.zone.run(() => {
      this.hideAddButton = true;
      setTimeout(() => {
        this.hideAddButton = false
      }, 500);
    });
  }
}
