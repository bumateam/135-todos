export interface TodoItem {
    id: number;
    name: string;
    type: TodoType;
    priority: number;
    status?: TodoStatusEnum;
}

export enum TodoType {
    BIG_TASK = 'BIG_TASK',
    MEDIUM_TASK = 'MEDIUM_TASK',
    SMALL_TASK = 'SMALL_TASK'
}

export enum TodoStatusEnum {
    NEW = 'NEW',
    FINISHED = 'FINISHED'
}