import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodosSmallTasksPage } from './todos-small-tasks';

@NgModule({
  declarations: [
    TodosSmallTasksPage,
  ],
  imports: [
    IonicPageModule.forChild(TodosSmallTasksPage),
  ],
})
export class TodosSmallTasksPageModule {}
