import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TodoStateService } from '../../shared/services/todo.service';
import { TodoItem, TodoType } from '../todos/todo.model';
import { TodoCountProgressMapper } from '../../components/todo-count-progress/todo-count-progress.mapper';
import { TodoCountProgressModel } from '../../components/todo-count-progress/todo-count-progress';

@IonicPage()
@Component({
  selector: 'page-todos-big-tasks',
  templateUrl: 'todos-big-tasks.html',
})
export class TodosBigTasksPage {

  public todos: TodoItem[];
  public todosCountProgress: TodoCountProgressModel;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private todoService: TodoStateService
  ) {
    let action = this.navParams.get('action')
    if (action === 'new') {
      console.log('new here');
    }
  }

  ionViewDidLoad() {
    this.todos = this.todoService.getTodos();
    if (this.todos) {
      this.todosCountProgress = TodoCountProgressMapper.toViewModel(this.todos, this.todoService.getTodoType());
    }
    console.log('ionViewDidLoad TodosBigTasksPage');
  }

}
