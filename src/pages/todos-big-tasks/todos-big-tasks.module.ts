import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodosBigTasksPage } from './todos-big-tasks';
import { TaskItemModule } from '../../components/task-item/task-item.module';
import { TodoCountProgressModule } from '../../components/todo-count-progress/todo-count-progress.module';

@NgModule({
  declarations: [
    TodosBigTasksPage,
  ],
  imports: [
    IonicPageModule.forChild(TodosBigTasksPage),
    TaskItemModule,
    TodoCountProgressModule,
  ],
})
export class TodosBigTasksPageModule {}
