import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-todos-medium-tasks',
  templateUrl: 'todos-medium-tasks.html',
})
export class TodosMediumTasksPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodosMediumTasksPage');
  }

}
