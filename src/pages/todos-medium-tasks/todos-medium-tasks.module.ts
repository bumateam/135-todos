import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodosMediumTasksPage } from './todos-medium-tasks';

@NgModule({
  declarations: [
    TodosMediumTasksPage,
  ],
  imports: [
    IonicPageModule.forChild(TodosMediumTasksPage),
  ],
})
export class TodosMediumTasksPageModule {}
