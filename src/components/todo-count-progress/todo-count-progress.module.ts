import { NgModule } from '@angular/core';
import { IonicModule } from "ionic-angular";
import { TodoCountProgressComponent } from './todo-count-progress';
import { NgCircleProgressModule } from "ng-circle-progress";

@NgModule({
	declarations: [TodoCountProgressComponent],
	imports: [IonicModule,
		NgCircleProgressModule.forRoot({
		  radius: 25,
		  outerStrokeWidth: 7,
		  innerStrokeWidth: 9,
		  backgroundPadding: -9,
		  space: -8,
		  titleFontSize: '14',
		  subtitleFontSize: '8',
		  titleColor: "#FFFFFF",
		  subtitleColor: "#FFFFFF",
		  outerStrokeColor: "#f4f4f4",
		  animationDuration: 300,
		  showUnits: false,
		  renderOnClick: false
		})],
	exports: [TodoCountProgressComponent]
})
export class TodoCountProgressModule {}
