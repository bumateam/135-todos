import { Component, Input } from '@angular/core';
import { TodoStateService } from '../../shared/services/todo.service';
import { TodoType } from '../../pages/todos/todo.model';

export interface TodoCountProgressModel {
  remaining: { title: string, percent: number }
  completed: { title: string, percent: number }
}

@Component({
  selector: 'todo-count-progress',
  templateUrl: 'todo-count-progress.html'
})
export class TodoCountProgressComponent {

  @Input() data: TodoCountProgressModel;
  
  strokeColor: string;

  constructor(private todoStateService: TodoStateService) {
  }

  ngOnInit() {
    this.getColorStyle();
  }

  private getColorStyle() {
    switch (this.todoStateService.getTodoType()) {
      case TodoType.BIG_TASK:
      this.strokeColor = '#c44b47';
      break;
      case TodoType.MEDIUM_TASK:
      this.strokeColor = '#2e7396';
      break;
      case TodoType.SMALL_TASK:
      this.strokeColor = '#49992a';
      break;
    }
  }
}
