import { TodoCountProgressModel } from './todo-count-progress';
import { TodoItem, TodoType, TodoStatusEnum } from '../../pages/todos/todo.model';

export class TodoCountProgressMapper {
    public static toViewModel(todos: TodoItem[], todoType: TodoType): TodoCountProgressModel {
        let result: TodoCountProgressModel;

        result = {
            remaining: {
                title: todos.filter(todo => todo.type === todoType && todo.status === TodoStatusEnum.NEW).length.toString(),
                percent: 55
            },
            completed: {
                title: todos.filter(todo => todo.type === todoType && todo.status === TodoStatusEnum.FINISHED).length.toString(),
                percent: 55
            }
        }

        return result;
    }
}