import { NgModule } from '@angular/core';
import { IonicModule } from "ionic-angular";
import { TaskItemComponent } from './task-item';
import { NgCircleProgressModule } from "ng-circle-progress";

@NgModule({
	declarations: [TaskItemComponent],
	imports: [IonicModule],
	exports: [TaskItemComponent]
})
export class TaskItemModule {}
