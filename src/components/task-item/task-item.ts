import { Component, Input } from '@angular/core';
import { TodoItem } from '../../pages/todos/todo.model';

@Component({
  selector: 'task-item',
  templateUrl: 'task-item.html'
})
export class TaskItemComponent {

	@Input()
  public title: string;
  
  @Input()
  public tasks: TodoItem[];

  constructor() {}

}
