import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Pages
import { WelcomePage } from '../pages/welcome/welcome';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { TodosPage } from '../pages/todos/todos';
import { TodosBigTasksPage } from '../pages/todos-big-tasks/todos-big-tasks';
import { TodosMediumTasksPage } from '../pages/todos-medium-tasks/todos-medium-tasks';
import { TodosSmallTasksPage } from '../pages/todos-small-tasks/todos-small-tasks';

// Providers
import { StartupProvider } from '../providers/shared/startup.provider';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{title: string, component: any, cachePreviousPage?: boolean, icon?: string, endOfGroup?: boolean}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private startupProvider: StartupProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: '1-3-5', component: TodosPage },
      { title: 'Big Tasks', component: TodosBigTasksPage, cachePreviousPage: true },
      { title: 'Medium Tasks', component: TodosMediumTasksPage, cachePreviousPage: true  },
      { title: 'Small Tasks', component: TodosSmallTasksPage, cachePreviousPage: true, endOfGroup: true },
      { title: 'Leave Feedback', component: null, cachePreviousPage: true, icon: 'information-circle' },
      { title: 'Rate the App', component: null, cachePreviousPage: true, icon: 'star'  },
      { title: 'Become a Pro', component: null, cachePreviousPage: true, icon: 'flower', endOfGroup: true  },
      { title: 'Settings', component: null, cachePreviousPage: true, icon: 'settings', endOfGroup: true  }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.showStartupPage();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    if (page.cachePreviousPage) {
      this.nav.push(page.component);
    } else {
      this.nav.setRoot(page.component);
    }
  }

  private async showStartupPage() {
    if (await this.startupProvider.showWelcome()) {
      this.rootPage = WelcomePage;
    } else if (await this.startupProvider.showTutorial()) {
      this.rootPage = TutorialPage;
    } else {
      this.rootPage = TodosPage;
    }
  }
}
