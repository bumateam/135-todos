import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Pages or Components
import { MyApp } from './app.component';

// Providers
import { StartupProvider } from '../providers/shared/startup.provider';
import { TodoStateService } from '../shared/services/todo.service';

// Strategies
import { TodoDefaultStrategy } from '../shared/services/todo-strategy/todo-default.strategy';
import { TodoBigStrategy } from '../shared/services/todo-strategy/todo-big.strategy';
import { TodoMediumStrategy } from '../shared/services/todo-strategy/todo-medium.strategy';
import { TodoSmallStrategy } from '../shared/services/todo-strategy/todo-small.strategy';

// Modules
import { WelcomePageModule } from '../pages/welcome/welcome.module';
import { TutorialPageModule} from '../pages/tutorial/tutorial.module';
import { TodosPageModule } from '../pages/todos/todos.module';
import { TodosBigTasksPageModule } from '../pages/todos-big-tasks/todos-big-tasks.module';
import { TodosMediumTasksPageModule } from '../pages/todos-medium-tasks/todos-medium-tasks.module';
import { TodosSmallTasksPageModule } from '../pages/todos-small-tasks/todos-small-tasks.module';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    WelcomePageModule,
    TutorialPageModule,
    TodosPageModule,
    TodosBigTasksPageModule,
    TodosMediumTasksPageModule,
    TodosSmallTasksPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StartupProvider,
    TodoDefaultStrategy,
    TodoBigStrategy,
    TodoMediumStrategy,
    TodoSmallStrategy,
    TodoStateService,
  ]
})
export class AppModule {}
